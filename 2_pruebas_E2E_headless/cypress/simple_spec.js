describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {        
	cy.visit('https://losestudiantes.co')
	//Punto 0
	cy.contains('Cerrar').click()
	cy.contains('Ingresar').click()
      	cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
      	cy.get('.cajaLogIn').find('input[name="password"]').click().type("12345")
      	cy.get('.cajaLogIn').contains('Ingresar').click()
     	cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })

    it('Sign up student ', function() {        
	//Punto 1 
	cy.get('.cajaSignUp').find('input[name="nombre"]').type("Edgar")
	cy.get('.cajaSignUp').find('input[name="apellido"]').type("Camacho")
	cy.get('.cajaSignUp').find('input[name="correo"]').type("ey.camacho10@uniandes.edu.co")
	cy.get('.cajaSignUp').find('input[name="password"]').type("12345")
	cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select("Administración")
	cy.get('.cajaSignUp').find('input[name="acepta"]').check()
	cy.get('.cajaSignUp').contains('Registrarse').click()
	cy.contains('Registro exitoso!')
    })

    it('Log in a student ', function() {        
	//Punto 1, Login de usuario que se crea 
	cy.visit('https://losestudiantes.co')
	cy.contains('Cerrar').click()
	cy.contains('Ingresar').click()
	cy.get('.cajaLogIn').find('input[name="correo"]').click().type("ey.camacho10@uniandes.edu.co")
      	cy.get('.cajaLogIn').find('input[name="password"]').click().type("12345")
      	cy.get('.cajaLogIn').contains('Ingresar').click()
	cy.contains('cuenta')
    })

    it('Create a user already exists ', function() {        
	//Punto 1, Login de usuario que se crea 
	cy.visit('https://losestudiantes.co')
	cy.contains('Cerrar').click()
	cy.contains('Ingresar').click()
	cy.get('.cajaSignUp').find('input[name="nombre"]').type("Edgar")
	cy.get('.cajaSignUp').find('input[name="apellido"]').type("Camacho")
	cy.get('.cajaSignUp').find('input[name="correo"]').type("ey.camacho10@uniandes.edu.co")
	cy.get('.cajaSignUp').find('input[name="password"]').type("12345")
	cy.get('.cajaSignUp').find('select[name="idDepartamento"]').select("Administración")
	cy.get('.cajaSignUp').find('input[name="acepta"]').check()
	cy.get('.cajaSignUp').contains('Registrarse').click()
	cy.contains('Ya existe un usuario registrado con el correo')
    })

    it('Find a teacher, select teacher ', function() {        
	//Punto 2, 3
	cy.visit('https://losestudiantes.co')
	cy.contains('Cerrar').click()
	cy.get('.Select-input').find("input").type("ingr", {force: true})
	cy.get('.Select-menu-outer').contains('Ingrid Quintana Guerrero').click();
	cy.get('.descripcionProfesor').contains('Ingrid Quintana Guerrero')
    })

    it('Find a subject ', function() {        
	//Punto 4
	cy.get('.materias').find('input[name="ARQU1420"]').check()
    })
})
