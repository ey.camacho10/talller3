'use strict';

const Audit = require('lighthouse').Audit;

const MAX_CARD_TIME_3_SEC = 3000;

class LoadAudit3 extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance_3sec',
            name: 'card-audit-3-secs',
            description: 'Schedule card initialized and ready at least 3 seconds ',
            failureDescription: 'Schedule Card slow to initialize at least 3 seconds',
            helpText: 'Used to measure time from navigationStart to when the schedule' +
            ' card is shown.',

            requiredArtifacts: ['TimeToCard']
        };
    }
	
	static audit(artifacts) {
        const loadedTime = artifacts.TimeToCard;

        const belowThreshold = loadedTime <= MAX_CARD_TIME_3_SEC;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit3;