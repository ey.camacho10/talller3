'use strict';

module.exports = {

    extends: 'proy_lighthouse:default',

    passes: [{
        passName: 'defaultPass',
        gatherers: [
            'card-gatherer'
        ]
    }],

    audits: [
        'card-audit',
		'card-audit-3-secs'
    ],

    categories: {
        ratp_pwa: {
            name: 'Ratp pwa metrics',
            description: 'Metrics for the ratp timetable site',
            audits: [
                {id: 'card-audit', weight: 1},
				{id: 'card-audit-3-secs', weight: 1}
            ]
        }
    }
};