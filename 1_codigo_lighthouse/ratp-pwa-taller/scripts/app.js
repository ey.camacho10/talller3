(function () {
    'use strict';

    var app = {
        isLoading: true,
        visibleCards: {},
        selectedTimetables: [],
        spinner: document.querySelector('.loader'),
        cardTemplate: document.querySelector('.cardTemplate'),
        container: document.querySelector('.main'),
        addDialog: document.querySelector('.dialog-container')
    };

    var indexedDB = window.indexedDB || window.webkitIndexedDB
                 || window.mozIndexedDB || window.msIndexedDB;


    /*****************************************************************************
     *
     * Event listeners for UI elements
     *
     ****************************************************************************/

    document.getElementById('butRefresh').addEventListener('click', function () {
        // Refresh all of the forecasts
        app.updateSchedules();
    });

    document.getElementById('butAdd').addEventListener('click', function () {
        // Open/show the add new city dialog
        app.toggleAddDialog(true);
    });

    document.getElementById('butAddCity').addEventListener('click', function () {


        var select = document.getElementById('selectTimetableToAdd');
        var selected = select.options[select.selectedIndex];
        var key = selected.value;
        var label = selected.textContent;
        if (!app.selectedTimetables) {
            app.selectedTimetables = [];
        }
        app.getSchedule(key, label);
        app.selectedTimetables.push({key: key, label: label});
        app.saveSelectedStations();
        app.toggleAddDialog(false);
    });

    document.getElementById('butAddCancel').addEventListener('click', function () {
        // Close the add new city dialog
        app.toggleAddDialog(false);
    });


    /*****************************************************************************
     *
     * Methods to update/refresh the UI
     *
     ****************************************************************************/

    // Toggles the visibility of the add new city dialog.
    app.toggleAddDialog = function (visible) {
        if (visible) {
            app.addDialog.classList.add('dialog-container--visible');
        } else {
            app.addDialog.classList.remove('dialog-container--visible');
        }
    };

    // Updates a weather card with the latest weather forecast. If the card
    // doesn't already exist, it's cloned from the template.

    app.updateTimetableCard = function (data) {
        var key = data.key;
        var dataLastUpdated = new Date(data.created);
        var schedules = data.schedules;
        var card = app.visibleCards[key];

        if (!card) {
            var label = data.label.split(', ');
            var title = label[0];
            var subtitle = label[1];
            card = app.cardTemplate.cloneNode(true);
            card.classList.remove('cardTemplate');
            card.querySelector('.label').textContent = title;
            card.querySelector('.subtitle').textContent = subtitle;
            card.removeAttribute('hidden');
            app.container.appendChild(card);
            app.visibleCards[key] = card;
        }
        card.querySelector('.card-last-updated').textContent = data.created;

        var scheduleUIs = card.querySelectorAll('.schedule');
        for(var i = 0; i<4; i++) {
            var schedule = schedules[i];
            var scheduleUI = scheduleUIs[i];
            if(schedule && scheduleUI) {
                scheduleUI.querySelector('.message').textContent = schedule.message;
            }
        }

        if (app.isLoading) {
            window.cardLoadTime = performance.now();
            app.spinner.setAttribute('hidden', true);
            app.container.removeAttribute('hidden');
            app.isLoading = false;
        }
    };

    /*****************************************************************************
     *
     * Methods for dealing with the model
     *
     ****************************************************************************/


    app.getSchedule = function (key, label) {
        var url = 'https://api-ratp.pierre-grimaud.fr/v3/schedules/' + key;

        if ('caches' in window) {
          caches.match(url).then(function(response) {
            if (response) {
              response.json().then(function updateFromCache(json) {
                var results = json.result;
                results.key = key;
                results.label = label;
                results.created = json._metadata.date;
                app.updateTimetableCard(results);
              });
            }
          });
        }

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    var response = JSON.parse(request.response);
                    var result = {};
                    result.key = key;
                    result.label = label;
                    result.created = response._metadata.date;
                    result.schedules = response.result.schedules;
                    app.updateTimetableCard(result);
                }
            } else {
                // Return the initial weather forecast since no data is available.
                app.updateTimetableCard(initialStationTimetable);
            }
        };
        request.open('GET', url);
        request.send();
    };

    // Iterate all of the cards and attempt to get the latest forecast data
    app.updateSchedules = function () {
        var keys = Object.keys(app.visibleCards);
        keys.forEach(function (key) {
            app.getSchedule(key);
        });
    };

    app.saveSelectedStations = function() {
      //while (openRequest.readyState != 'done'){
      //}
      var openRequestSave = indexedDB.open('stationsDB');

      openRequestSave.onsuccess = function (e) {
        var bd = openRequestSave.result;
        var tx = bd.transaction("stations", "readwrite");
        var store = tx.objectStore("stations");

        var selectedStations = app.selectedTimetables;
        for (var obj in selectedStations){
          var resultado = store.put(selectedStations[obj]);
        }
        tx.oncomplete = function() {
          bd.close();
        };
      };

      openRequestSave.onerror = function (e)  {
          alert('Error guardando en la base de datos');
      };
      //localStorage.selectedTimetables = selectedStations;
    };


    /*
     * Fake weather data that is presented when the user first uses the app,
     * or when the user has not saved any cities. See startup code for more
     * discussion.
     */

    var initialStationTimetable = {

        key: 'metros/1/bastille/A',
        label: 'Bastille, Direction La Défense',
        created: '2017-07-18T17:08:42+02:00',
        schedules: [
            {
                message: '0 mn'
            },
            {
                message: '2 mn'
            },
            {
                message: '5 mn'
            }
        ]


    };


    /************************************************************************
     *
     * Code required to start the app
     *
     * NOTE: To simplify this codelab, we've used localStorage.
     *   localStorage is a synchronous API and has serious performance
     *   implications. It should not be used in production applications!
     *   Instead, check out IDB (https://www.npmjs.com/package/idb) or
     *   SimpleDB (https://gist.github.com/inexorabletash/c8069c042b734519680c)
     ************************************************************************/
     var openRequest = indexedDB.open('stationsDB');

     openRequest.onupgradeneeded = function() {
       console.log("onupgradeneeded ");
       var baseD = openRequest.result;
       var store = baseD.createObjectStore('stations',{ keyPath : 'key'});
       store.createIndex('by_label', 'label', { unique : false });
     };

     openRequest.onsuccess = function (e) {
       console.log(" openRequest.onsuccess ");
       var bd = openRequest.result;
       var tx = bd.transaction("stations", "readonly");
       var store = tx.objectStore("stations");
       var requestSelect = store.getAll();
       requestSelect.onsuccess = function(evt) {
         var cursor = evt.target.result;
         for (var ind in cursor) {
           app.selectedTimetables.push(cursor[ind]);
         }
         //app.selectedTimetables = localStorage.selectedTimetables;
         if (app.selectedTimetables && app.selectedTimetables.length > 0) {
           //app.selectedTimetables = JSON.parse(app.selectedTimetables);
           app.selectedTimetables.forEach(function(station) {
             app.getSchedule(station.key, station.label);
           });
         } else {
           app.updateTimetableCard(initialStationTimetable);
           app.selectedTimetables = [
               {key: initialStationTimetable.key, label: initialStationTimetable.label}
           ];
           app.saveSelectedStations();
         }
       };
     };

     openRequest.onerror = function (e)  {
         alert('Error cargando la base de datos');
     };

    if ('serviceWorker' in navigator) {
      /*navigator.serviceWorker
               .register('./service-worker.js')
               .then(function() { console.log('Service Worker Registered'); });
               */
      navigator.serviceWorker
               .register('./sw.js')
               .then(function() { console.log('Service Worker sw-precache Registered'); });

    }
})();
